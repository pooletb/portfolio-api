import uuid
from functools import reduce


def generate_id():

    retval = uuid.uuid4()
    return str(retval)


def format_errors(errors):

    return (
        errors[0] if len(errors) == 1
        else reduce(
            lambda accumulated_value, update_value: "{}  {}".format(accumulated_value, update_value),
            ["{}. {}".format(i, error) for i, error in enumerate(errors, start=1)]
        )
    )
