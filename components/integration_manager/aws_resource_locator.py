import boto3
from botocore.exceptions import ClientError

import logging
from config import get_config_base
from exceptions import SystemConfigurationException

_LOGGER = logging.get_logger(__name__)


def locate_aws_secret(logical_name):

    return _locate_resource("secrets", logical_name)


def locate_dynamodb_table(logical_name):

    return _locate_resource("dynamodbTableNames", logical_name)


def locate_s3_bucket(logical_name):

    return _locate_resource("s3BucketNames", logical_name)


def locate_lambda_function(logical_name):

    return _locate_resource("lambdaFunctionNames", logical_name)


def _locate_resource(resource_type, logical_name):

    config_base = get_config_base()
    if not isinstance(config_base, str) or not config_base.strip():
        raise SystemConfigurationException(
            "Could not locate AWS \"{}\" resource \"{}\": Config base not set.".format(
                resource_type,
                logical_name
            )
        )
    aws_systems_manager = boto3.client("ssm")
    system_parameter_name = "{base_path}/{sub_path}/{name}".format(
        base_path=config_base,
        sub_path=resource_type,
        name=logical_name
    )
    try:
        systems_manager_response = aws_systems_manager.get_parameter(Name=system_parameter_name)
    except ClientError as e:
        _LOGGER.error(
            "Failed to retrieve the AWS system parameter named \"%s\": %s",
            system_parameter_name,
            e
        )
        return None
    return systems_manager_response["Parameter"].get("Value")
