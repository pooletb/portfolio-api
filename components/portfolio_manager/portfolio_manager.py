import json
import logging
from datetime import datetime

import boto3
from boto3.dynamodb.conditions import Key, Attr
from botocore.exceptions import ClientError

from integration_manager.dynamodb_integration import locate_dynamodb_table
from integration_manager.rest_integration import Response
from integration_manager.util import generate_id, format_errors


logger = logging.getLogger()


def get_all_portfolio_entries_by_type(type):

  portfolio_entries_table = _get_dynamodb_table()
  response = portfolio_entries_table.scan(
      FilterExpression=Attr('type').eq(type)
  )

  def get_timestamp(portfolio_entry):
    return portfolio_entry.get('timestamp', '')

  if response is None or response.get('Items') is None:
    logger.info('No items were returned by DynamoDB.')
    portfolio_entries = []
  else:
    portfolio_entries = sorted(response.get('Items'), key=get_timestamp, reverse=True)
  return Response(success=True, detail=portfolio_entries)


def create_new_portfolio_entry(portfolio_entry):

  portfolio_entries_table = _get_dynamodb_table()
  portfolio_entry_id = generate_id()
  now = datetime.now()
  portfolio_entry['id'] = portfolio_entry_id
  portfolio_entry['timestamp'] = now.strftime('%m/%d/%Y, %H:%M:%S')
  try:
    portfolio_entries_table.put_item(
      Item=portfolio_entry,
    )
  except ClientError as e:
    logger.error(
      'Failed to insert PortfolioEntry item into DynamoDB: %s.  Response information: %s',
      e,
      e.response
    )
    return Response(success=False, detail='Unable to persist the PortfolioEntry item.')
  return Response(success=True, detail=portfolio_entry_id)


def _get_dynamodb_table():

    dynamodb = boto3.resource('dynamodb')
    instance_table_name = locate_dynamodb_table('portfolioEntries')
    return dynamodb.Table(instance_table_name)