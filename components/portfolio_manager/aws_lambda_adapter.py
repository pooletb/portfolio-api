import json
import logging
from json import JSONDecodeError

from integration_manager.aws_lambda_integration import encode_response
from integration_manager.rest_integration import Response
from . import portfolio_manager

#
#
logger = logging.getLogger()


def get_all_portfolio_entries_by_type(event, context):

  request_query = event["queryStringParameters"]
  if "type" in request_query:
    type = request_query["type"]
  else:
    return encode_response(Response(success=False, detail="\"type\" query parameter is required."))

  return encode_response(portfolio_manager.get_all_portfolio_entries_by_type(type))


def create_new_portfolio_entry(event, context):
  portfolio_entry = None
  request_body = event.get('body')
  try:
    portfolio_entry = json.loads(request_body)
  except JSONDecodeError:
    logger.warning('Request body is not valid JSON: %s', request_body)
    return encode_response(Response(success=False, detail='Request body is not valid JSON'))
  return encode_response(portfolio_manager.create_new_portfolio_entry(portfolio_entry))
